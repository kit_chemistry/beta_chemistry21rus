//SOUNDS
var audio = [], typingText = [];

//TIMEOUTS
var timeout = [];

//AWARDs NUMBER
var awardNum = 0;

//OXYGEN AMOUNT 
var oxygenAmount = 0,
	oxygenPointer;

var drawPointer = function()
{
	switch(oxygenAmount)
	{
		case 0: oxygenPointer.css("transform", "rotate(-75deg)");
				break;
		case 10: oxygenPointer.css("transform", "rotate(-65deg)");
				break;
		case 20: oxygenPointer.css("transform", "rotate(-50deg)");
				break;
		case 30: oxygenPointer.css("transform", "rotate(-35deg)");
				break;
		case 40: oxygenPointer.css("transform", "rotate(-20deg)");
				break;
		case 50: oxygenPointer.css("transform", "rotate(-5deg)");
				break;
		case 60: oxygenPointer.css("transform", "rotate(10deg)");
				break;
		case 70: oxygenPointer.css("transform", "rotate(25deg)");
				break;
		case 80: oxygenPointer.css("transform", "rotate(40deg)");
				break;
		case 90: oxygenPointer.css("transform", "rotate(55deg)");
				break;
		case 100: oxygenPointer.css("transform", "rotate(70deg)");
				break;
	}
}

var allHaveHtml = function(jqueryElement){
	for(var i = 0; i < jqueryElement.length; i ++)
	{
		if(!$(jqueryElement[i]).html())
			return false;
	}
	return true;
}

var fadeOneByOne = function(jqueryElement, curr, interval, endFunction)
{
	if (curr < jqueryElement.length)
	{
		timeout[curr] = setTimeout(function(){
			$(jqueryElement[curr]).fadeIn(200);
			fadeOneByOne(jqueryElement, ++curr, interval, endFunction);
		}, interval);
	}
	else
		endFunction();
}

function TypingText(jqueryElement, interval, hasSound, endFunction)
{
	this.text = jqueryElement.html();
	this.jqueryElement = jqueryElement;
	this.interval = interval;
	this.currentSymbol = 0;
	this.endFunction = endFunction;
	this.hasSound = hasSound;
	this.audio = new Audio("audio/keyboard-sound.mp3");
	this.audio.addEventListener("ended", function(){
			this.play();
		});
	
	this.timeout;
	this.jqueryElement.html("");
}

TypingText.prototype.write = function()
{
	tObj = this;
	if (tObj.audio.paused && tObj.hasSound) {
		tObj.audio.play();
	}
	tObj.timeout = setTimeout(function(){
		if (tObj.text[tObj.currentSymbol] === "/" && tObj.text[tObj.currentSymbol + 1] === 'n')
		{
			tObj.jqueryElement.append("<br>");
			tObj.currentSymbol += 2;
		}
		else if (tObj.text[tObj.currentSymbol] === "/" && tObj.text[tObj.currentSymbol + 1] === 's')
		{
			tObj.currentSymbol += 2;
			tObj.jqueryElement.append("<sup>" + tObj.text[tObj.currentSymbol] + "</sup>");
			tObj.currentSymbol ++;
		}
		else if (tObj.text[tObj.currentSymbol] === "/" && tObj.text[tObj.currentSymbol + 1] === 'b')
		{
			tObj.currentSymbol += 2;
			tObj.jqueryElement.append("<b>" + tObj.text[tObj.currentSymbol] + "</b>");
			tObj.currentSymbol ++;
		}
		else
		{
			tObj.jqueryElement.append(tObj.text[tObj.currentSymbol]);
			tObj.currentSymbol ++;
		}
		if (tObj.currentSymbol < tObj.text.length) 
			tObj.write();
		else
		{
			tObj.audio.pause();
			tObj.endFunction();
		}
	}, tObj.interval);
}

TypingText.prototype.stopWriting = function()
{
	clearTimeout(this.timeout);
	this.audio.pause();
}

var loadImages = function(){
	jQuery.get('fileNames.txt', function(data) {
		var imagesSrc = data.split("\n"),
			images = [],
			loadPercentage = 0,
			imagesNum = imagesSrc.length - 1;
			unitToAdd = Math.round(100 / imagesNum);
		
		var imageLoadListener = function(){
			loadPercentage += unitToAdd;
			console.log(loadPercentage);
			imagesNum --;
			if (loadPercentage >= 100) {
				//hideEverythingBut($("#frame-000"));
			}
		};
		
		for (var i = 0; i < imagesSrc.length - 1; i ++)
		{
			images[i] = new Image();
			images[i].src = "pics/" + imagesSrc[i];
			images[i].addEventListener("load", imageLoadListener);
		}
	});
}

function DragTask(jqueryElements, successCondition, successFunction, failFunction, finishCondition, finishFunction)
{
	this.draggables = jqueryElements;
	this.draggabillies = [];
	this.vegetable;
	this.basket;
	
	this.makeThemDraggable = function()
	{
		for(var i = 0; i < this.draggables.length; i++)
			this.draggabillies[i] = new Draggabilly(this.draggables[i]);
	}
	
	this.addEventListeners = function()
	{
		for(var i = 0; i < this.draggabillies.length; i++)
		{
			this.draggabillies[i].on("dragStart", this.onStart);
			this.draggabillies[i].on("dragEnd", this.onEnd);
		}
	}
	
	this.onEnd = function(instance, event, pointer)
	{
		var currVeg = this.vegetable;
		var currBasket = this.basket;
		currVeg.fadeOut(0);
		currBasket = $(document.elementFromPoint(pointer.pageX, pointer.pageY));
		currVeg.fadeIn(0);
		
		if (currBasket.attr("data-key") && successCondition(currVeg.attr("data-key"), currBasket.attr("data-key")))
				successFunction(currVeg, currBasket);
		else
			failFunction(currVeg, currBasket);
		
		currVeg.removeClass("box-shadow-white");
		currVeg.css("opacity", "");
		
		if (finishCondition())
		{
			finishFunction();
		}
	}
	
	this.onStart = function(instance, event, pointer)
	{
		this.vegetable = $(event.target);
		this.vegetable.css("z-index", "9999");
		this.vegetable.addClass("box-shadow-white");
		this.vegetable.css("opacity", "0.6");
	}
	
	this.makeThemDraggable();
	this.addEventListeners();
}

var blink = function(jqueryElements, interval, times)
{
	var intervalHalf = Math.round(interval/2);
	timeout[0] = setTimeout(function(){
		jqueryElements.css("outline", "3px solid red");
		timeout[1] = setTimeout(function(){
			jqueryElements.css("outline", "");
			if (times) 
				blink(jqueryElements, interval, --times)		
		}, intervalHalf);
	}, intervalHalf);
}

var launch000 = function()
{
	
}

var launch101 = function()
{
		theFrame = $("#frame-101"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		text = $(prefix + ".text-container"), 
		o2 = $(".o2");
	
	typingText[0] = new TypingText(text, 50, true, function(){
		timeout[0] = setTimeout(function(){
			fadeNavsIn();
		}, 5000);
	});
	
	o2.fadeOut(0);
	fadeNavsIn(0);
		
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			typingText[0].write();
		}, 2000);
		timeout[1] = setTimeout(function(){
			o2.fadeIn(0);
			timeout[2] = setTimeout(function(){
				o2.css({
					"width": "40%",
					"height": "46%",
					"left": "30%",
					"top": "25%"
				});
			}, 1000);
			
			timeout[3] = setTimeout(function(){
				oxygenAmount += 100;
				drawPointer();
			}, 3000);
			
			timeout[4] = setTimeout(function(){
				oxygenAmount -= 100;
				drawPointer();
			}, 6000);
			
			timeout[5] = setTimeout(function(){
				o2.css({
					"width": "",
					"height": "",
					"left": "",
					"top": ""
				});
			}, 8000);
		}, 7000);
	};
}

var launch102 = function()
{
		theFrame = $("#frame-102"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		ship = $(prefix + ".ship"),
		blackSkin = $(prefix + ".black-skin");
	
	audio[0] = new Audio("audio/spacecraft-sound.mp3");
	
	audio[0].addEventListener("ended", function(){
		hideEverythingBut($("#frame-103"));
	});
	
	shipSprite = new Motio(ship[0], {
		"fps": "1.5", 
		"frames": "5"
	});
	
	shipSprite.on("frame", function(){
		if(this.frame === this.frames - 1)
		{
			this.pause();
		}	
	});
	
	oxygenAmount = 100;
	drawPointer();
	
	ship.fadeOut(0);
	fadeNavsIn();
	
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			audio[0].play();
			ship.fadeIn(500);
			blackSkin.addClass("transition-5s");
			timeout[1] = setTimeout(function(){
				shipSprite.play();
			}, 1000);
			timeout[2] = setTimeout(function(){
				blackSkin.css("opacity", "1");
			}, 2000);
		}, 1000);
	};
}

var launch103 = function()
{
		theFrame = $("#frame-103"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		words = $(prefix + ".word"), 
		atomStructure = $(prefix + ".atom-structure-rus, "+prefix + ".atom-structure-kaz, " + prefix + ".atom-structure-eng"),
		protonNumber = $(prefix + ".proton-number-rus, " + prefix + ".proton-number-kaz, " + prefix + ".proton-number-eng"),
		isotope = $(prefix + ".isotope-rus, " + prefix + ".isotope-kaz, " + prefix + ".isotope-eng"),
		massNumber = $(prefix + ".mass-number-rus, " + prefix + ".mass-number-kaz, " + prefix + ".mass-number-eng"),
		nuclei = $(prefix + ".nuclei-rus, " + prefix + ".nuclei-kaz, " + prefix + ".nuclei-eng");
	
	audio[0] = new Audio("audio/sci-fi.mp3");
	audio[1] = new Audio("audio/s3-1.mp3");
	
	audio[0].addEventListener("ended", function(){
		this.play();
	});
	audio[1].addEventListener("ended", function(){
		audio[0].pause();
		timeout[0] = setTimeout(function(){
			fadeNavsIn();
		}, 1000);
	});
	
	oxygenAmount -= 10;
	drawPointer();
	words.css("left", "2000px");
	fadeNavsIn();
	
	startButtonListener = function(){
		fadeNavsOut();
		audio[0].play();
		words.addClass("transition-5s");
		atomStructure.css("left", "");
		timeout[0] = setTimeout(function(){
			audio[1].play();
		}, 1000);
		timeout[1] = setTimeout(function(){
			protonNumber.css("left", "");
		}, 5000);
		timeout[2] = setTimeout(function(){
			isotope.css("left", "");
		}, 7000);
		timeout[3] = setTimeout(function(){
			massNumber.css("left", "");
		}, 9000);
		timeout[3] = setTimeout(function(){
			nuclei.css("left", "");
		}, 11000);
	};
}

var launch104 = function()
{
		theFrame = $("#frame-104"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		labels = $(prefix + ".labels"),
		items = $(prefix + ".item");
	
	audio[0] = new Audio("audio/s4-1.mp3");
	
	audio[0].addEventListener("ended", function(){
		timeout[0] = setTimeout(function(){
			fadeNavsIn();
		}, 3000);
	});
	
	labels.fadeOut(0);
	items.fadeOut(0);
	fadeNavsIn();
	
	oxygenAmount -= 10;
	drawPointer();
			
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			audio[0].play();
			timeout[1] = setTimeout(function(){
				items.fadeIn(1000);
				timeout[0] = setTimeout(function(){
					labels.fadeIn(1000);
				}, 1000);
			}, 3000);
		}, 1000);
	};
}

var launch105 = function()
{
		theFrame = $("#frame-105"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		label = $(prefix + ".label"),
		labelImage = $(prefix + ".label-image"),
		portrait = $(prefix + ".portrait");
	
	audio[0] = new Audio("audio/s5-1.mp3");
	
	audio[0].addEventListener("ended", function(){
		items.fadeIn(1000);
		audio[1].play();
		timeout[0] = setTimeout(function(){
			labels.fadeIn(1000);
		}, 1000);
	});
	
		typingText[0] = new TypingText(label, 50, true, function(){
		timeout[1] = setTimeout(function(){
			fadeNavsIn();
		}, 3000);
	});
	
	label.fadeOut(0);
	portrait.fadeOut(0);
	labelImage.fadeOut(0);
	fadeNavsIn();
	
	oxygenAmount -= 10;
	drawPointer();
			
		startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			audio[0].play();
			label.fadeIn(500);
			labelImage.fadeIn(1000);
			typingText[0].write();
			timeout[0] = setTimeout(function(){
				portrait.fadeIn(500);
			}, 2000);
		}, 1000);
	};
}

var launch106 = function()
{
		theFrame = $("#frame-106"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		cloud = $(prefix + ".s6-stroenie"),
		electrons = $(prefix + ".electrons");
	
	var electronSprite = new Motio(electrons[0], {
		"fps": "2", 
		"frames": "3"
	});
	
	audio[0] = new Audio("audio/s6-1.mp3");
	
	audio[0].addEventListener("ended", function(){
		timeout[0] = setTimeout(function(){
			fadeNavsIn();
		}, 2000);
	});
	
	cloud.fadeOut(0);
	fadeNavsIn();
	
	oxygenAmount -= 10;
	drawPointer();
			
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			audio[0].play();
		}, 2000);
		timeout[1] = setTimeout(function(){
			cloud.fadeIn(1000);
		}, 8000);
		timeout[2] = setTimeout(function(){
			electronSprite.play();
		}, 18000);
	};
}

var launch107 = function()
{
		theFrame = $("#frame-107"),
		theClone = theFrame.clone();
	var prefix = "#" + theFrame.attr("id") + " ",
		items = $(prefix + ".item");
	
	audio[0] = new Audio("audio/s7-1.mp3");
	
	audio[0].addEventListener("ended", function(){
		timeout[0] = setTimeout(function(){
			fadeNavsIn();
		}, 3000);
	});
	
	items.fadeOut(0);
	fadeNavsIn();
	
	oxygenAmount -= 10;
	drawPointer();
			
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			audio[0].play();
			fadeOneByOne(items, 0, 1000, function(){});
		}, 1000);
	};
}

var launch108 = function()
{
		theFrame = $("#frame-108"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		area = $(prefix + ".area"),
		nucleonNumber = $(prefix + ".nucleon-number"),
		nucleonNumberLabel = $(prefix + ".nucleon-number .label"),
		k = $(prefix + ".K");
	
		typingText[0] = new TypingText(area, 100, false, function(){
		
	}); 
	
	var nucleonNumberSprite = new Motio(nucleonNumber[0], {
		"fps": "1",
		"frames": "4"
	});
	nucleonNumberSprite.on("frame", function(){
		if(this.frame === this.frames - 1)
		{
			nucleonNumberLabel.css("color", "white");
			this.pause();
		}
	});
	
	nucleonNumberSprite.to(3, true);
	
	audio[0] = new Audio("audio/s8-1.mp3");
	
	audio[0].addEventListener("ended", function(){
		timeout[0] = setTimeout(function(){
			fadeNavsIn();
		}, 3000);
	});
	
	fadeNavsIn();
	nucleonNumberLabel.css("color", "transparent");
	nucleonNumber.fadeOut(0);
	area.fadeOut(0);	
	
	oxygenAmount -= 10;
	drawPointer();
			
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			area.fadeIn(500);
			typingText[0].write();		
			audio[0].play();
		}, 1000);
		timeout[0] = setTimeout(function(){
			area.fadeOut(0);
			k.fadeOut(0);
			nucleonNumber.fadeIn(1000);
			nucleonNumberSprite.play();
		}, 28000);
	};
}

var launch109 = function()
{
		theFrame = $("#frame-109"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		task = $(prefix + ".task"),
		text = $(prefix + ".text"),
		balls = $(prefix + ".ball"),
		periodicTable = $(prefix + ".periodic-table"),
		helpButton = $(prefix + ".help-button"),
		keepThinking = $(prefix + ".keep-thinking"),
		hint = $(prefix + ".hint");

		typingText[0] = new TypingText(task, 50, true, function(){
		timeout[0] = setTimeout(function(){
			balls.fadeIn(500);
			text.fadeIn(500);
			task.fadeOut(0);
		}, 1000);
	});
	
	var successCondition = function(vegetable, basket)
	{
		return vegetable === basket;
	};
	
	var successFunction = function(vegetable, basket)
	{
		basket.html(vegetable.html());
		basket.css("color", vegetable.css("color"));
		basket.css("font-size", vegetable.css("font-size"));
		basket.css("font-style", vegetable.css("font-style"));
		vegetable.remove();
		oxygenAmount += 10;
		drawPointer();
	};
	
	var failFunction = function(vegetable, basket)
	{
		vegetable.css("left", "");
		vegetable.css("top", "");
		vegetable.css("z-index", "");
		keepThinking.fadeIn(0);
		timeout[1] = setTimeout(function(){
			keepThinking.fadeOut(0);
		}, 3000);
	};
	
	var finishCondition = function(vegetable, basket)
	{
		return $(prefix + ".ball").length <= 11;
	};
	
	var finishFunction = function(vegetable, basket)
	{
		hint.fadeIn(1000);
		audio[3].play();
		timeout[0] = setTimeout(function(){
			fadeNavsIn();
			audio[4].play();
		}, 7000);
	};
	
	var helpListener = function(){
		if(parseInt(periodicTable.css("height")))
			periodicTable.css("height", "0%");
		else
			periodicTable.css("height", "100%");
	};
	
	helpButton.off("click", helpListener);
	helpButton.on("click", helpListener);
	
	var dragTask = new DragTask(balls, successCondition, successFunction, failFunction, finishCondition, finishFunction);
	
	audio[0] = new Audio("audio/alarm-sound.mp3");
	audio[1] = new Audio("audio/sci-fi.mp3");
	audio[2] = new Audio("audio/s9-1.mp3");
	audio[3] = new Audio("audio/s11-1.mp3");
	audio[4] = new Audio("audio/s12-1.mp3");
	
	audio[1].addEventListener("ended", function(){
		audio[1].play();
	});	
	audio[2].addEventListener("ended", function(){
		timeout[1] = setTimeout(function(){
			typingText[0].write();
			audio[1].play();
		}, 1000);
	});
	
	fadeNavsIn();
	balls.fadeOut(0);
	text.fadeOut(0);
	hint.fadeOut(0);
	periodicTable.css("height", "0%");
	periodicTable.css("opacity", "0.9");
	keepThinking.fadeOut(0);
	
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			periodicTable.addClass("transition-2s");
			oxygenAmount -= 10;
			drawPointer();
			audio[0].volume = 0.2;
			audio[1].volume = 0.2;
			audio[0].play();
			audio[2].play();
		}, 1000);
	};
}

var launch113 = function()
{
		theFrame = $("#frame-113"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		text = $(prefix + ".text"),
		blank = $(prefix + ".blank"),
		task = $(prefix + ".task"),
		balls = $(prefix + ".ball"), 
		checkButton = $(prefix + ".check-button"), 
		helpButton = $(prefix + ".help-button"),
		periodicTable = $(prefix + ".periodic-table"),
		answer = $(prefix + ".answer"),
		b1proton = 0,
		b1neutron = 0,
		b2proton = 0,
		b2neutron = 0, 
		b3proton = 0, 
		b3neutron = 0;
	
	typingText[0] = new TypingText(text, 50, true, function(){
		timeout[0] = setTimeout(function(){
			audio[0].play();
			task.fadeIn(500);
			blank.fadeIn(500);
			helpButton.fadeIn(500);
			balls.fadeIn(500);
		}, 2000);
	});
	
	audio[0] = new Audio("audio/s13-1.mp3");
	audio[1] = new Audio("audio/s15-1.mp3");
	
	audio[1].addEventListener("ended", function(){
		fadeNavsIn();
	});
	
	//DragTask(jqueryElements, successCondition, successFunction, failFunction, finishCondition, finishFunction)
	var successCondition = function(vegetable, basket)
	{
		return basket === "basket";
	}
	var successFunction = function(vegetable, basket)
	{
		
		var temp = vegetable.clone();
		temp.removeClass("box-shadow-white");
		temp.css("z-index", "2");
		$(prefix).append(temp);
		vegetable.css("left", "");
		vegetable.css("top", "");
		vegetable.css("z-index", "");
		
		if(basket.hasClass("basket-1"))
		{
			if(vegetable.hasClass("proton"))
				b1proton ++;
			else
				b1neutron ++;
		}
		
		if(basket.hasClass("basket-2"))
		{
			if(vegetable.hasClass("proton"))
				b2proton ++;
			else
				b2neutron ++;
		}
		
		if(basket.hasClass("basket-3"))
		{
			if(vegetable.hasClass("proton"))
				b3proton ++;
			else
				b3neutron ++;
		}
		
		console.log("b1proton: " + b1proton);
		console.log("b1neutron: " + b1neutron);
		console.log("b2proton: " + b2proton);
		console.log("b2neutron: " + b2neutron);
		console.log("b3proton: " + b3proton);
		console.log("b3neutron: " + b3neutron);
		
		if((b1proton || b1neutron) &&
			(b2proton || b2neutron) &&
			(b3proton || b3neutron))
			checkButton.fadeIn(500);
	}
	var failFunction = function(vegetable, basket)
	{
		vegetable.css("left", "");
		vegetable.css("top", "");
		vegetable.css("z-index", "");
	}
	var finishCondition = function()
	{
	}
	var finishFunction = function()
	{
	}
	
	var dragTask = new DragTask(balls, successCondition, successFunction, failFunction, finishCondition, finishFunction);
	
	fadeNavsIn();
	answer.fadeOut(0);
	blank.fadeOut(0);
	balls.fadeOut(0);
	task.fadeOut(0);
	helpButton.fadeOut(0);
	text.fadeOut(0);
	checkButton.fadeOut(0);
	periodicTable.css("height", "0%");
	
	var helpListener = function(){
		if(parseInt(periodicTable.css("height")))
			periodicTable.css("height", "0%");
		else
			periodicTable.css("height", "100%");
	};
	helpButton.off("click", helpListener);
	helpButton.on("click", helpListener);
	
	var checkButtonListener = function(){
		answer.fadeIn(0);
		if(b1proton === 4)
			oxygenAmount += 5;
		if(b1neutron === 5)
			oxygenAmount += 5;
		if(b2proton === 8)
			oxygenAmount += 5;
		if(b2neutron === 8)
			oxygenAmount += 5;
		if(b2proton === 12)
			oxygenAmount += 5;
		if(b2neutron === 12)
			oxygenAmount += 5;
		
		drawPointer();
		audio[1].play();
	}
	checkButton.off("click", checkButtonListener);
	checkButton.on("click", checkButtonListener);
	
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			text.fadeIn(500);
			periodicTable.addClass("transition-2s");
			typingText[0].write();
		}, 1000);
	};
}

var launch117 = function()
{
		theFrame = $("#frame-117"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		text = $(prefix + ".text"),
		task = $(prefix + ".task"),
		table = $(prefix + ".table"),
		inputs = $(prefix + "input"),
		checkButton = $(prefix + ".check-button"),
		helpButton = $(prefix + ".help-button"),
		periodicTable = $(prefix + ".periodic-table");
	
	audio[0] = new Audio("audio/sci-fi.mp3");
	audio[1] = new Audio("audio/s17-1.mp3");
	
	audio[0].addEventListener("ended", function(){
		this.play();
	});
	
	audio[1].addEventListener("ended", function(){
		timeout[0] = setTimeout(function(){
			task.fadeIn(500);
			typingText[0].write();
		}, 2000);
	});
	
	typingText[0] = new TypingText(task, 50, true, function(){
		table.fadeIn(500);
		helpButton.fadeIn(0);
	});
	
	inputs.val("");
	
	fadeNavsIn();
	oxygenAmount -= 10;
	drawPointer();
	checkButton.fadeOut(0);
	text.fadeOut(0);
	table.fadeOut(0);
	task.fadeOut(0);
	periodicTable.css("height", "0%");
	helpButton.fadeOut(0);
	
	var helpListener = function(){
		if(parseInt(periodicTable.css("height")))
			periodicTable.css("height", "0%");
		else
			periodicTable.css("height", "100%");
	};
	helpButton.off("click", helpListener);
	helpButton.on("click", helpListener);
	
	var inputListener = function(){
		var emptyNum = 14;
		for(var i = 0; i < inputs.length; i++)
			if($(inputs[i]).val())
				emptyNum --;
			
		if(!emptyNum)
			checkButton.fadeIn(500);
	};
	inputs.on("keyup", inputListener); //jquery 
	
	var checkButtonListener = function(){
		for(var i = 0; i < inputs.length; i++)
		{
			if($(inputs[i]).val() === $(inputs[i]).attr("data-correct"))
			{
				oxygenAmount += 5;
				$(inputs[i]).css("color", "green");
			}
			else
			{
				$(inputs[i]).css("color", "red");
			}
			drawPointer();
		}
		
		timeout[2] = setTimeout(function(){
			checkButton.fadeOut(0);
			for(var i = 0; i < inputs.length; i++)
			{
				if($(inputs[i]).val() === $(inputs[i]).attr("data-correct"))
				{
					$(inputs[i]).css("color", "");
					$(inputs[i]).css("font-weight", "bold");
					$(inputs[i]).css("font-size", "1.3vmax");
				}
				else
				{
					$(inputs[i]).css("color", "");
					$(inputs[i]).css("color", "normal");
					$(inputs[i]).val($(inputs[i]).attr("data-correct"));
					$(inputs[i]).css("font-size", "1.1vmax");
				}
			}
		}, 3000);
		
		timeout[3] = setTimeout(function(){
			fadeNavsIn();
		}, 8000);
		
		drawPointer();
	}
	checkButton.off("click", checkButtonListener);
	checkButton.on("click", checkButtonListener);
	
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			periodicTable.addClass("transition-2s");
			text.fadeIn(1000);
			audio[0].volume = 0.3;
			audio[0].play();
			audio[1].play();	
		}, 1000);
	};
}

var launch121 = function()
{
		theFrame = $("#frame-121"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		helpButton = $(prefix + ".help-button"),
		periodicTable = $(prefix + ".periodic-table"),
		balls = $(prefix + ".ball"),
		keepThinking = $(prefix + ".keep-thinking"),
		task = $(prefix + ".task"),
		textContainer = $(prefix + ".text-container"), 
		helpButton = $(prefix + ".help-button"),
		periodicTable = $(prefix + ".periodic-table");
	
	audio[0] = new Audio("audio/s21-1.mp3");
	audio[1] = new Audio("audio/s23-1.mp3");
	
	audio[0].addEventListener("ended", function(){
		typingText[0].write();
		task.fadeIn(500);
	});
	
	var helpListener = function(){
		if(parseInt(periodicTable.css("height")))
			periodicTable.css("height", "0%");
		else
			periodicTable.css("height", "100%");
	};
	helpButton.off("click", helpListener);
	helpButton.on("click", helpListener);
	
	typingText[0] = new TypingText(task, 50, true, function(){
		balls.fadeIn(500);
		textContainer.fadeIn(500);
		helpButton.fadeIn(500);
	});
	
	var successCondition = function(vegetable, basket)
	{
		return vegetable === basket;
	};
	
	var successFunction = function(vegetable, basket)
	{
		basket.html(vegetable.html());
		basket.css("color", vegetable.css("color"));
		basket.css("font-size", vegetable.css("font-size"));
		basket.css("font-style", vegetable.css("font-style"));
		vegetable.remove();
		oxygenAmount += 5;
		drawPointer();
	};
	
	var failFunction = function(vegetable, basket)
	{
		vegetable.css("left", "");
		vegetable.css("top", "");
		vegetable.css("z-index", "");
		keepThinking.fadeIn(0);
		timeout[1] = setTimeout(function(){
			keepThinking.fadeOut(0);
		}, 2000);
	};
	
	var finishCondition = function(vegetable, basket)
	{
		return $(prefix + ".ball").length <= 4;
	};
	
	var finishFunction = function(vegetable, basket)
	{
		drawPointer();
		audio[1].play();
		timeout[0] = setTimeout(function(){
			fadeNavsIn();
		}, 2000);
	};
	
	var dragTask = new DragTask(balls, successCondition, successFunction, failFunction, finishCondition, finishFunction);
	
	oxygenAmount -= 10;
	fadeNavsIn();
	keepThinking.fadeOut(0);
	textContainer.fadeOut(0);
	task.fadeOut(0);
	balls.fadeOut(0);
	helpButton.fadeOut(0);
	periodicTable.css("height", "0%");
		
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			periodicTable.addClass("transition-2s");
			audio[0].play();
		}, 1000);
	};
}

var launch124 = function()
{
		theFrame = $("#frame-124"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		text = $(prefix + ".text"),
		task = $(prefix + ".task"),
		table = $(prefix + ".table"),
		inputs = $(prefix + "input"),
		checkButton = $(prefix + ".check-button"),
		helpButton = $(prefix + ".help-button"),
		periodicTable = $(prefix + ".periodic-table"),
		kalii = $(prefix + ".kalii"),
		natrii = $(prefix + ".natrii");
	
	audio[0] = new Audio("audio/sci-fi.mp3");
	audio[1] = new Audio("audio/s24-1.mp3");
	audio[3] = new Audio("audio/s24_0_aliens_talking.mp3");
	audio[4] = new Audio("audio/s27-1.mp3");
	
	audio[0].addEventListener("ended", function(){
		this.play();
	});
	
	audio[1].addEventListener("ended", function(){
		audio[3].pause();
		task.fadeIn(500);
		typingText[0].write();
	});
	
	audio[4].addEventListener("ended", function(){
		fadeNavsIn();
	});
		
	typingText[0] = new TypingText(task, 50, true, function(){
		table.fadeIn(500);
		helpButton.fadeIn(0);
	});
	
	inputs.val("");
	fadeNavsIn();
	oxygenAmount -= 10;
	drawPointer();
	checkButton.fadeOut(0);
	text.fadeOut(0);
	table.fadeOut(0);
	task.fadeOut(0);
	periodicTable.css("height", "0%");
	helpButton.fadeOut(0);
	kalii.fadeOut(0);
	natrii.fadeOut(0);
	
	var helpListener = function(){
		if(parseInt(periodicTable.css("height")))
			periodicTable.css("height", "0%");
		else
			periodicTable.css("height", "100%");
	};
	helpButton.off("click", helpListener);
	helpButton.on("click", helpListener);
	
	var inputListener = function(){
		var emptyNum = 14;
		for(var i = 0; i < inputs.length; i++)
			if($(inputs[i]).val())
				emptyNum --;
			
		if(!emptyNum)
			checkButton.fadeIn(500);
	};
	inputs.on("keyup", inputListener); //jquery 
	
	var checkButtonListener = function(){
		for(var i = 0; i < inputs.length; i++)
		{
			if($(inputs[i]).val() === $(inputs[i]).attr("data-correct"))
			{
				oxygenAmount += 5;
				$(inputs[i]).css("color", "green");
			}
			else
			{
				$(inputs[i]).css("color", "red");
			}
			drawPointer();
		}
		
		timeout[2] = setTimeout(function(){
			checkButton.fadeOut(0);
			for(var i = 0; i < inputs.length; i++)
			{
				if($(inputs[i]).val() === $(inputs[i]).attr("data-correct"))
				{
					$(inputs[i]).css("color", "");
					$(inputs[i]).css("font-weight", "bold");
					$(inputs[i]).css("font-size", "1.3vmax");
				}
				else
				{
					$(inputs[i]).css("color", "");
					$(inputs[i]).css("color", "normal");
					$(inputs[i]).val($(inputs[i]).attr("data-correct"));
					$(inputs[i]).css("font-size", "1.1vmax");
				}
			}
		}, 3000);
		
		timeout[3] = setTimeout(function(){
			table.fadeOut(0);
			task.fadeOut(0);
			helpButton.fadeOut(0);
			checkButton.fadeOut(0);
			audio[4].play();
			timeout[4] = setTimeout(function(){
				kalii.fadeIn(500);
			}, 8000);
			timeout[5] = setTimeout(function(){
				kalii.fadeOut(500);
				natrii.fadeIn(500);
			}, 12000);
		}, 8000);
		
		drawPointer();
	}
	checkButton.off("click", checkButtonListener);
	checkButton.on("click", checkButtonListener);
	
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			periodicTable.addClass("transition-2s");
			text.fadeIn(1000);
			audio[0].volume = 0.3;
			audio[3].volume = 0.3;
			audio[3].play();
			audio[1].play();
		}, 1000);
	};
}

var launch127 = function()
{
		theFrame = $("#frame-127"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		astronautApproach = $(prefix + ".astronaut-approach"),
		astronautWave = $(prefix + ".astronaut-wave"),
		shipLeaving = $(prefix + ".ship-leaving");
	
	audio[0] = new Audio("audio/spacecraft-sound.mp3");
	audio[1] = new Audio("audio/s28-1.mp3");
	
	shipLeavingSprite = new Motio(shipLeaving[0], {
		"fps": "2",
		"frames": "7"
	});
	
	shipLeavingSprite.on("frame", function(){
		if(this.frame === this.frames - 1)
		{
			this.pause();
			timeout[1] = setTimeout(function(){
				fadeNavsIn();
				audio[0].pause();
			}, 2000);
		}
	});
	
	astronautApproachSprite = new Motio(astronautApproach[0], {
		"fps": "3", 
		"frames": "6"
	});
	astronautApproachSprite.on("frame", function(){
		if(this.frame === this.frames - 1)
		{
			this.pause();
			audio[0].volume = 0.4;
			audio[0].play();
			astronautApproach.fadeOut(0);
			astronautWave.fadeIn(0);
			astronautWaveSprite.play();
			timeout[0] = setTimeout(function(){
				astronautWaveSprite.pause();
				astronautWave.fadeOut(0);
				theFrame.css("background-image", "url(pics/s27-bg-2.png)");
				shipLeaving.fadeIn(0);
				shipLeavingSprite.play();
			}, 3000);
		}	
	});
	
	astronautWaveSprite = new Motio(astronautWave[0], {
		"fps": "3", 
		"frames": "3"
	});
	
	astronautWave.fadeOut(0);
	shipLeaving.fadeOut(0);
	fadeNavsIn(0);
	
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			audio[1].play();
			astronautApproachSprite.play();
		}, 2000);
	};
}

var launch201 = function()
{
		theFrame = $("#frame-201"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		taskBG = $(prefix + ".task-bg"),
		balls = $(prefix + ".ball"),
		keepThinking = $(prefix + ".keep-thinking"),
		task = $(prefix + ".title");
		
		typingText[0] = new TypingText(task, 50, true, function(){
		balls.fadeIn(500);
		taskBG.fadeIn(500);
	});
	
	var successCondition = function(vegetable, basket)
	{
		return vegetable === basket;
	};
	
	var successFunction = function(vegetable, basket)
	{
		basket.html(vegetable.html());
		vegetable.remove();
		oxygenAmount += 10;
		drawPointer();
	};
	
	var failFunction = function(vegetable, basket)
	{
		vegetable.css("left", "");
		vegetable.css("top", "");
		vegetable.css("z-index", "");
		keepThinking.fadeIn(0);
		timeout[1] = setTimeout(function(){
			keepThinking.fadeOut(0);
		}, 3000);
	};
	
	var finishCondition = function(vegetable, basket)
	{
		return $(prefix + ".ball").length == 0;
	};
	
	var finishFunction = function(vegetable, basket)
	{
		fadeNavsIn();
	};
	
	var dragTask = new DragTask(balls, successCondition, successFunction, failFunction, finishCondition, finishFunction);
	
	keepThinking.fadeOut(0);
	balls.fadeOut(0);
	taskBG.fadeOut(0);
	fadeNavsIn();
	
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			taskBG.fadeIn(500);
			typingText[0].write();
		}, 1000);
	};
}

var launch202 = function()
{
		theFrame = $("#frame-202"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		taskBG = $(prefix + ".task-bg"),
		balls = $(prefix + ".ball"),
		keepThinking = $(prefix + ".keep-thinking"),
		task = $(prefix + ".title"),
		table = $(prefix + ".table");
		
		typingText[0] = new TypingText(task, 50, true, function(){
		balls.fadeIn(500);
		taskBG.fadeIn(500);
		table.fadeIn(500);
	});
	
	var successCondition = function(vegetable, basket)
	{
		return vegetable === basket;
	};
	
	var successFunction = function(vegetable, basket)
	{
		basket.html(vegetable.html());
		vegetable.remove();
		oxygenAmount += 10;
		drawPointer();
	};
	
	var failFunction = function(vegetable, basket)
	{
		vegetable.css("left", "");
		vegetable.css("top", "");
		vegetable.css("z-index", "");
		keepThinking.fadeIn(0);
		timeout[1] = setTimeout(function(){
			keepThinking.fadeOut(0);
		}, 3000);
	};
	
	var finishCondition = function(vegetable, basket)
	{
		return $(prefix + ".ball").length == 0;
	};
	
	var finishFunction = function(vegetable, basket)
	{
		fadeNavsIn();
	};
	
	var dragTask = new DragTask(balls, successCondition, successFunction, failFunction, finishCondition, finishFunction);
	
	keepThinking.fadeOut(0);
	balls.fadeOut(0);
	taskBG.fadeOut(0);
	fadeNavsIn();
	table.fadeOut(0);
	
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			taskBG.fadeIn(500);
			typingText[0].write();
		});
	};
}

var launch203 = function()
{
	    theFrame = $("#frame-203"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		downloadButton = $(prefix + ".download"),
		textarea = $(prefix + "textarea"),
		title = $(prefix + ".title");
		
		typingText[0] = new TypingText(title, 100, true, function(){
		textarea.fadeIn(500);
	});
	
	var downloadButtonListener = function(){
		var blob = new Blob([textarea.val()], {type: "text/plain;charset=utf-8"});
		saveAs(blob, "Мое Сочинение.txt");
		fadeNavsIn();
	};
	downloadButton.off("click", downloadButtonListener);
	downloadButton.on("click", downloadButtonListener);
	
	textarea.val("");
	title.fadeOut(0);
	fadeNavsIn();
	
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			title.fadeIn(500);
			typingText[0].write();
		}, 1000);
	};
}

var hideEverythingBut = function(elem)
{
	var frames = $(".frame");
	
	frames.fadeOut(0);
	elem.fadeIn(0);
	
	$(".o2").fadeIn(0);
	
	for (var i = 0; i < audio.length; i++)
		audio[i].pause();	

	for (var i = 0; i < timeout.length; i++)
		clearTimeout(timeout[i]);
	
	for (var i = 0; i < typingText.length; i++)
		typingText[i].stopWriting();
	
	if(elem.hasClass("fact"))
		$(".o2").fadeOut(0);
	
	switch(elem.attr("id"))
	{
		case "frame-000":
			$(".o2").fadeOut(0);
			launch000();
			fadeNavsOut();
			break;
		case "frame-101":
			launch101();
			break;
		case "frame-102":
			launch102();
			break;
		case "frame-103":
			launch103();
			break;
		case "frame-104":
			launch104();
			break;
		case "frame-105":
			launch105();
			break;
		case "frame-106":
			launch106();
			break;
		case "frame-107":
			launch107();
			break;
		case "frame-108":
			launch108();
			break;
		case "frame-109":
			launch109();
			break;
		case "frame-113":
			launch113();
			break;
		case "frame-117":
			launch117();
			break;
		case "frame-121":
			launch121();
			break;
		case "frame-124":
			launch124();
			break;
		case "frame-127":
			launch127();
			break;
		case "frame-201":
			launch201();
			fadeLauncherOut();
			break;
		case "frame-202":
			launch202();
			fadeLauncherOut();
			break;
		case "frame-203":
			launch203();
			fadeLauncherOut();
			break;
	}
}

var initMenuButtons = function(){
	var links = $(".link");
	links.click(function(){
		var elem = $("#"+$(this).attr("data-link"));
		hideEverythingBut(elem);
	});
};

var main = function()
{
	oxygenPointer = $(".o2-pointer");
	initMenuButtons();
	//hideEverythingBut($("#pre-load"));
	hideEverythingBut($("#frame-000"));
	//loadImages();
};

$(document).ready(main);